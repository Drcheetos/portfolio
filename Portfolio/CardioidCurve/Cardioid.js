let points;
let r;
let g;
let b = 50;
let factor;
let factorSlider;
let pointSlider;
let radius;

function setup() {
    createCanvas(900, 900);
    radius = width / 2 - 20;
    factorSlider = createSlider(2, 400, 2, 0.1);
    factorSlider.position(10, 10);
    pointSlider = createSlider(1, 500, 200, 1);
    pointSlider.position(10, 40);
}

function draw() {
    background(0);
    points = pointSlider.value();
    factor = factorSlider.value();
    fill(255);
    noStroke();
    text("Factor = " + factor, factorSlider.x + factorSlider.width, 20);
    text("Number Of Points = " + points, pointSlider.x + pointSlider.width, 47);

    let tempFactor = factor % 20;
    r = map(tempFactor, 2, 20, 0, 255);
    g = map(tempFactor, 20, 2, 0, 255);

    translate(width / 2, height / 2);
    stroke(r, g, b);
    noFill();
    ellipse(0, 0, radius * 2);

    for (let i = 0; i < points; i++) {
        let temp = getVector(i);
        fill(r, g, b);
        noStroke();
        ellipse(temp.x, temp.y, 16);
    }
    
    for (let i = 0; i < points; i++) {
        let aVect = getVector(i);
        let bVect = getVector(i * factor);
        stroke(0, r, g);
        strokeWeight(1);
        line(aVect.x, aVect.y, bVect.x, bVect.y);
        stroke(r, g, b, 75);
        strokeWeight(8);
        line(aVect.x, aVect.y, bVect.x, bVect.y);
    }
}

function getVector(index) {
    let angle = map(index, 0, points, 0, TWO_PI);
    let v = p5.Vector.fromAngle(angle + PI);
    v.mult(radius);
    return v;
}